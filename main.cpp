#include "mainwindow.h"
#include "wrapper.h"
#include "sniffer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
    Wrapper testRunner("../testcases/testcases");
    Wrapper ethernetTest("./scripts/ethernet.py");
    Wrapper wireshark("wireshark");
    Sniffer sniffer("./scripts/sniffer.py");

    QObject::connect(&w, &MainWindow::ethernetStart, &ethernetTest, &Wrapper::start);
    QObject::connect(&ethernetTest, &Wrapper::stdoutMsg, &w, &MainWindow::writeEthernetOutput);
    QObject::connect(&ethernetTest, &Wrapper::stderrMsg, &w, &MainWindow::writeEthernetOutput);

    QObject::connect(&w, &MainWindow::testStart, &testRunner, &Wrapper::start);
    QObject::connect(&w, &MainWindow::testStop, &testRunner, &Wrapper::stop);
    QObject::connect(&testRunner, &Wrapper::stdoutMsg, &w, &MainWindow::writeTestOutput);
    QObject::connect(&testRunner, &Wrapper::stderrMsg, &w, &MainWindow::writeTestError);

    QObject::connect(&w, &MainWindow::snifferStart, &sniffer, &Sniffer::start);
    QObject::connect(&w, &MainWindow::snifferStop, &sniffer, &Sniffer::stop);
    QObject::connect(&w, &MainWindow::snifferStop, &sniffer, &Sniffer::clearContainer);
    QObject::connect(&sniffer, &Sniffer::stderrMsg, &w, &MainWindow::writeSnifferOutput);
    QObject::connect(&sniffer, &Sniffer::packetArrived, &w, &MainWindow::logNewPacket);
    QObject::connect(&w, &MainWindow::requestPacket, &sniffer, &Sniffer::getDataframe);
    QObject::connect(&sniffer, &Sniffer::sendDataframe, &w, &MainWindow::printPacket);
    QObject::connect(&sniffer, &Sniffer::wrapperStarting, &w, &MainWindow::snifferStarting);
    QObject::connect(&sniffer, &Sniffer::wrapperNotRunning, &w, &MainWindow::snifferNotRunning);


    QObject::connect(&w, &MainWindow::wiresharkStart, &wireshark, &Wrapper::start);

    w.show();

    return a.exec();
}
