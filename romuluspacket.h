#ifndef ROMULUSPACKET_H
#define ROMULUSPACKET_H

#include <QObject>

class RomulusPacket
{
public:
    RomulusPacket();
    void assign(QStringList list);

    QString DLC;
    QString IDSOURCE;
    QString SEQID;
    QString CMDCODE;
    QString NP;
    QString DATA;
    QString CHKSUM;
};

#endif // ROMULUSPACKET_H
