#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    credentialsValid(false)
{
    ui->setupUi(this);
    setWindowTitle("MARTIANS - Man Accessible ROMULUSlib Tests for Interface And Network Sniffer");

    settingsFile = "martians.ini";
    loadSettings();

    dumpfile = "/tmp/dumpfile";

    setupWidgets();
    validateFields();
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::closeEvent( QCloseEvent *event )
{
    saveSettings();
    event->accept();
}

void MainWindow::loadSettings()
{
    QSettings settings(settingsFile, QSettings::NativeFormat);

    QString ip = settings.value("ip", "127.0.0.1").toString();
    QString port1 = settings.value("port1", "8888").toString();
    QString port2 = settings.value("port2", "8889").toString();
    QString jsonfile = settings.value("jsonFile", "").toString();

    ui->lineAddr->setText(ip);
    ui->linePort1->setText(port1);
    ui->linePort2->setText(port2);
    ui->lineTestFile->setText(jsonfile);
}

void MainWindow::saveSettings()
{
    QSettings settings(settingsFile, QSettings::NativeFormat);
    settings.setValue("ip", ui->lineAddr->text());
    settings.setValue("port1", ui->linePort1->text());
    settings.setValue("port2", ui->linePort2->text());
    settings.setValue("jsonFile", ui->lineTestFile->text());
}

void MainWindow::setupWidgets()
{
    ui->tabWidget->setTabText(0, "Testing");
    ui->tabWidget->setTabText(1, "Ethernet");
    ui->tabWidget->setTabText(2, "Protocol");
    ui->tabWidget->setCurrentIndex(0);

    setupTestingWidgets();
    setupNetworkingWidgets();
    setupEthernetTestWidgets();
    setupSnifferWidgets();

}

void MainWindow::setupNetworkingWidgets()
{
    ui->labelAddr->setText("Address");
    ui->labelPort1->setText("Protocol port");
    ui->labelPort2->setText("Monitoring port");
    ui->groupBox->setTitle("Networking credentials");

    //textfields validators
    QString regexString = "^(([1-9]?\\d|1\\d\\d|2[0-5][0-5]|2[0-4]\\d)\\.){3}([1-9]?\\d|1\\d\\d|2[0-5][0-5]|2[0-4]\\d)$";
    QRegExp ipRegex = QRegExp(regexString);
    QRegExpValidator* addressValidator = new QRegExpValidator(ipRegex);
    ui->lineAddr->setValidator(addressValidator);
    QObject::connect(ui->lineAddr, SIGNAL(textChanged(QString)), this, SLOT(validateFields()));

    QIntValidator* portValidator = new QIntValidator(1, 65535);
    ui->linePort1->setValidator(portValidator);
    QObject::connect(ui->linePort1, SIGNAL(textChanged(QString)), this, SLOT(validateFields()));

    QIntValidator* port2Validator = new QIntValidator(1, 65535);
    ui->linePort2->setValidator(port2Validator);
    QObject::connect(ui->linePort2, SIGNAL(textChanged(QString)), this, SLOT(validateFields()));

}

void MainWindow::setupSnifferWidgets()
{
    ui->groupBoxSniffer->setTitle("Sniffing");
    ui->buttonSniffer->setText("Run Sniffer");
    ui->buttonStopSniffer->setText("Stop");
    ui->buttonStopSniffer->setEnabled(false);
    ui->buttonWireshark->setText("Analyze in Wireshark");

    ui->labelEth->setText("Interface");
    ui->labelFilter->setText("BPF filter (optional)");


    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Packet"));
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);

    ui->groupBox_3->setTitle("ROMULUS data frame");
    ui->label_start->setText("START");
    ui->label_dlc->setText("DLC");
    ui->label_idsource->setText("ID SOURCE");
    ui->label_seqid->setText("SEQ ID");
    ui->label_cmdcode->setText("CMD CODE");
    ui->label_np->setText("NP");
    ui->label_data->setText("DATA");
    ui->label_chksum->setText("CHKSUM");
    ui->label_end->setText("END");

    QStringList interfaces = getAllInterfaces();
    ui->comboBox->addItems(interfaces);

    connect(ui->buttonSniffer, &QPushButton::clicked, this, &MainWindow::snifferStartClicked);
    connect(ui->buttonStopSniffer, &QPushButton::clicked, this, &MainWindow::snifferStopClicked);
    connect(ui->tableWidget, &QTableWidget::currentCellChanged, this, &MainWindow::tableSelected);
    connect(ui->buttonWireshark, &QPushButton::clicked, this, &MainWindow::wiresharkStartClicked);
}

void MainWindow::addTableRow(int number)
{
    int rowCount = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(rowCount);

    const QString data = QString::number(number);
    QTableWidgetItem* cell = new QTableWidgetItem;
    cell->setText(data);
    ui->tableWidget->setItem(rowCount,0,cell);
}

void MainWindow::clearTable()
{
    while (ui->tableWidget->rowCount() > 0) {
        ui->tableWidget->removeRow(0);
    }
}

QStringList MainWindow::getAllInterfaces()
{
    QNetworkInterface interfaceManager;
    auto interfaces = interfaceManager.allInterfaces();

    QStringList interfacesNames;

    foreach (QNetworkInterface interface, interfaces) {
        interfacesNames.append(interface.name());
    }

    return interfacesNames;
}


void MainWindow::setupEthernetTestWidgets()
{
    ui->buttonClear->setText("Clear");
    ui->buttonStart->setText("Start");

    auto lambdaClear = [this](){this->ui->fieldEthernet->clear();};
    QObject::connect(ui->buttonClear, &QPushButton::clicked, lambdaClear);

    QObject::connect(ui->buttonStart, &QPushButton::clicked,
                     this, &MainWindow::ethernetStartClicked);

}

void MainWindow::setupTestingWidgets()
{
    ui->pushButtonRunTests->setText("Start Test");
    ui->pushButtonStopTests->setText("Stop");
    ui->pushButtonClear->setText("Clear");
    ui->pushButtonSelectFile->setText("Select JSON");

    connect(ui->pushButtonRunTests, &QPushButton::clicked, this, &MainWindow::testStartClicked);
    connect(ui->pushButtonStopTests, &QPushButton::clicked, this, &MainWindow::testStopClicked);
    connect(ui->pushButtonSelectFile, &QPushButton::clicked, this, &MainWindow::selectJson);

    auto lambdaClear = [this](){this->ui->plainTestLogOut->clear();
                                this->ui->plainTestLogErr->clear();};

    QObject::connect(ui->pushButtonClear, &QPushButton::clicked, lambdaClear);
}

void MainWindow::ethernetStartClicked()
{
    QString ipAddr = ui->lineAddr->text();
    QString port1 = ui->linePort1->text();
    QString port2 = ui->linePort2->text();

    QString ports = QString("%1,%2").arg(port1, port2);

    QStringList ethernetArgs;
    ethernetArgs.append(ipAddr);
    ethernetArgs.append(ports);

    emit ethernetStart(ethernetArgs);

}

void MainWindow::writeEthernetOutput(QString output)
{
    ui->fieldEthernet->appendPlainText(output);
}

void MainWindow::snifferStartClicked()
{
    QString interface = ui->comboBox->currentText();
    QString bpfFilter = ui->lineFilter->text();
    QStringList snifferArgs;

    snifferArgs.append("-i"); // interface option
    snifferArgs.append(interface);

    snifferArgs.append("-dump"); // interface option
    snifferArgs.append(dumpfile);

    snifferArgs.append("-bpf"); // interface option
    snifferArgs.append(bpfFilter);

    emit snifferStart(snifferArgs);
    clearTable();
}

void MainWindow::snifferStopClicked()
{
    emit snifferStop();
}

void MainWindow::writeSnifferOutput(QString output)
{
    ui->fieldSniffer->appendPlainText(output);
}

void MainWindow::snifferStarting()
{
    ui->buttonSniffer->setEnabled(false);
    ui->buttonStopSniffer->setEnabled(true);
}

void MainWindow::snifferNotRunning()
{
    ui->buttonSniffer->setEnabled(true);
    ui->buttonStopSniffer->setEnabled(false);
}

void MainWindow::wiresharkStartClicked()
{
    QStringList wiresharkArgs;
    wiresharkArgs.append(dumpfile);
    emit wiresharkStart(wiresharkArgs);
}

void MainWindow::logNewPacket(int number)
{
    addTableRow(number);
}

void MainWindow::tableSelected()
{
    int row = ui->tableWidget->currentRow();
    qDebug()<<row;
    emit requestPacket(row);
}

void MainWindow::printPacket(QStringList fields)
{
    if(fields.size() == 7)
    {
        ui->lineDlc->setText(fields.at(0));
        ui->lineIDSource->setText(fields.at(1));
        ui->lineSeqID->setText(fields.at(2));
        ui->lineCmdCode->setText(fields.at(3));
        ui->lineNp->setText(fields.at(4));
        ui->lineData->clear();
        ui->lineData->appendPlainText(fields.at(5));
        ui->lineChksum->setText(fields.at(6));
    }
}

void MainWindow::validateFields()
{
    QString address = ui->lineAddr->text();
    QString port1 = ui->linePort1->text();
    QString port2 = ui->linePort2->text();

    if(port1 == "0") ui->linePort1->setText("");
    if(port2 == "0") ui->linePort2->setText("");
    int pos = 0;

    QValidator::State addrState  = ui->lineAddr->validator()->validate(address, pos);

    bool areValid = (addrState == QValidator::Acceptable) && !port1.isEmpty() && !port2.isEmpty();
    ui->buttonStart->setEnabled(areValid);
    ui->buttonSniffer->setEnabled(areValid);

}

void MainWindow::testStartClicked()
{
    QStringList arguments;
    QString ipAddr = ui->lineAddr->text();
    QString port1 = ui->linePort1->text();
    QString json = ui->lineTestFile->text();

    arguments.append(ipAddr);
    arguments.append(port1);
    arguments.append(json);

    emit testStart(arguments);
}

void MainWindow::testStopClicked()
{
    emit testStop();
}

void MainWindow::writeTestOutput(QString output)
{
    ui->plainTestLogOut->appendPlainText(output);
}

void MainWindow::writeTestError(QString output)
{
    ui->plainTestLogErr->appendPlainText(output);
}

void MainWindow::selectJson()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Image"));
    ui->lineTestFile->setText(filename);
}
