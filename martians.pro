#-------------------------------------------------
#
# Project created by QtCreator 2018-09-12T22:45:25
#
#-------------------------------------------------

QT      += core gui network
CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = martians
TEMPLATE = app

MAIN_DIR = /usr/bin/MARTIANS
INSTALLATION_DIR = $${MAIN_DIR}/app
LINK_DIR = /usr/bin
ENTRY = $${TARGET}.desktop
ENTRY_DIR = /usr/share/applications

testcases.path = /usr/bin/MARTIANS/testcases
testcases.files += ../../testcases/testcases

paramfile.path = /usr/bin/MARTIANS/mars
paramfile.files += ../../mars/parameters.json

target.path = $${INSTALLATION_DIR}
target.files = $${TARGET} martians.ini tests.json martians-icon.svg

target.commands +=  sudo ln -s $${INSTALLATION_DIR}/$${TARGET} $${LINK_DIR}; \
                    sudo cp $${ENTRY} $${ENTRY_DIR}; \
                    sudo cp -R scripts $${INSTALLATION_DIR}/scripts; \

QMAKE_INSTALL_FILE = install -m 777

target.uninstall += sudo unlink $${LINK_DIR}/$${TARGET}; \
                    sudo rm $${ENTRY_DIR}/$${ENTRY}; \
                    sudo rm -f -r $${INSTALLATION_DIR}/scripts/*; \
                    sudo rmdir $${INSTALLATION_DIR}/scripts;

target.depends = install_testcases install_paramfile


INSTALLS += testcases paramfile target



LIBS += -lpthread
LIBS += -L../../.. -lROMULUS
INCLUDEPATH += ../../../include
INCLUDEPATH += ../common_include


SOURCES += main.cpp\
    mainwindow.cpp \
    sniffer.cpp \
    romuluspacket.cpp

#Define common sources
SOURCES += ../common_src/wrapper.cpp \

HEADERS  += mainwindow.h \
    sniffer.h \
    romuluspacket.h

#Define common headers
HEADERS += ../common_include/wrapper.h \

FORMS    += mainwindow.ui
