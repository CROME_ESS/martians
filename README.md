# MARTIANS

## Synopsis
MARTIANS (Man Accessible ROMULUS Tests for Interface And Network Sniffer) is a set of useful tools for testing and verifying the communication protocol
itself as well as the networking capabilities of the device. Therefore, it allows the user to specify the parametrization test using a JSON file, with a proper test scenario to test the network status of the device (currently using the nmap program; however, it can be easily changed to any other tool or custom user program, in the form of a complied program or a script). To check the protocol itself, MARTIANS is integrated with the network sniffer program that intercepts the protocol packets and presents them to the user as the proper fields of the protocol frame. It is also possible to see all the network communication within Wireshark.

# Getting Started
## Required Dependencies

|      Name | Version  |                       Type-link    |
|----------:|---------:|-----------------------------------:|
|ROMULUSlib |   6.0    |                                lib |
|Qt         |   5.x    |   Qt framework (https://www.qt.io) |
|Python3    |   3.0    |                                    |
|Python3 PIP|   19.2   |      https://pip.pypa.io/en/stable |
|testcases  |   1.0    |                                Bin |
|Nmap       |   7.01   |             (https://www.nmap.org) |
|Wireshark  |   x.x    |        (https://www.wireshark.org) |
|Tshark     |   x.x    |                                app |
|PyShark    |   x.x    |https://pypi.python.org/pypi/pyshark|

It has been built and tested successfully with gcc 4.8.5 

## Compilation
compilation can be done as follows :

    $ qmake vulcan.pro
    $ make

Check if user has privileges to Wireshark/tshark dumpfile:

/usr/bin/dumpcap

Create a temporary file : /tmp/dumpfile:

touch /tmp/ dumpcap
    
The project should then build successfully. In the case of an error with qmake itself, it may be necessary to specify the qmake version:

    $ qmake -qt=qt5 vulcan.pro

## Note

As the applications, use external programs the following steps should be completed for proper use :
 Building testcases exporting the bin to $PATH,

