#include "romuluspacket.h"

RomulusPacket::RomulusPacket()
{

}

void RomulusPacket::assign(QStringList fields)
{
    DLC     = fields.at(0);
    IDSOURCE= fields.at(1);
    SEQID   = fields.at(2);
    CMDCODE = fields.at(3);
    NP      = fields.at(4);
    DATA    = fields.at(5);
    CHKSUM  = fields.at(6);
}
