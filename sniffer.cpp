#include "sniffer.h"
#include "romuluspacket.h"

Sniffer::Sniffer(QString prog)
    : Wrapper(prog)
{
    counter = 0;
    connect(this, &Wrapper::stdoutMsg, this, &Sniffer::addPacket);
}

QStringList Sniffer::parseFrame(QString data)
{
    QPair<int, QStringList> newPacket;

    QStringList splitData= data.split("-");
    splitData.removeFirst();

    return splitData;
}

void Sniffer::addPacket(QString data)
{
    QStringList packetData;
    packetData = parseFrame(data);

    RomulusPacket packet;
    packet.assign(packetData);

    //qDebug()<<packetData;
    ++counter;

    packets.insert(counter, packetData);
    emit packetArrived(counter);

}

void Sniffer::clearContainer()
{
    counter = 0;
    packets.clear();
}


void Sniffer::getDataframe(int num)
{
    emit sendDataframe(packets[num]);
}

