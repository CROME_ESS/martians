#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkInterface>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    bool credentialsValid;
    QString dumpfile;
    QString settingsFile;

    void loadSettings();
    void saveSettings();
    void closeEvent(QCloseEvent*);

    void setupWidgets();
    void setupEthernetTestWidgets();
    void setupTestingWidgets();
    void setupNetworkingWidgets();
    void setupSnifferWidgets();
    QStringList getAllInterfaces();

signals:
    void testStart(QStringList);
    void testStop();
    void ethernetStart(QStringList);
    void snifferStart(QStringList);
    void snifferStop();
    void wiresharkStart(QStringList);

    void requestPacket(int);

public slots:
    void validateFields();

    void testStartClicked();
    void testStopClicked();
    void writeTestOutput(QString);
    void writeTestError(QString);
    void selectJson();

    void ethernetStartClicked();
    void writeEthernetOutput(QString);

    void snifferStartClicked();
    void snifferStopClicked();
    void writeSnifferOutput(QString);
    void snifferStarting();
    void snifferNotRunning();
    void wiresharkStartClicked();
    void logNewPacket(int);
    void tableSelected();
    void printPacket(QStringList);
    void addTableRow(int number);
    void clearTable();


};

#endif // MAINWINDOW_H
