#!/usr/bin/python3

import sys
import subprocess

def test_ping(hostname):
    status = subprocess.call(["ping -c 1 " + hostname], stdout=subprocess.PIPE, shell=True)
    proc  = subprocess.Popen(["ping -c 1 " + hostname], stdout=subprocess.PIPE, shell=True)
    (output, err) = proc.communicate()

    return (status, output.decode("utf-8"))

def test_nmap(hostname, ports):
    status = subprocess.call(["nmap -p " + ports + " " + hostname], stdout=subprocess.PIPE, shell=True)
    proc  = subprocess.Popen(["nmap -p " + ports + " " + hostname], stdout=subprocess.PIPE, shell=True)
    (output, err) = proc.communicate()

    return (status, output.decode("utf-8"))


if __name__ == "__main__":

    hostname = sys.argv[1]
    ports = sys.argv[2]

    (code, output) = test_nmap(hostname, ports)
    sys.stdout.write(output)

    exit(code)


