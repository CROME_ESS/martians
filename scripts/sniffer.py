#!/usr/bin/python3

import os
import sys
import pyshark
import argparse
import binascii as ba

# eth = 'enp0s31f6'
# wifi = 'wlp1s0'
# lo = 'lo'



def start_sniffing(interf=None, bpf=None, dumpfile=None):
    capture = pyshark.LiveCapture(interface=interf, bpf_filter=bpf, output_file=dumpfile)
    capture.output_file = dumpfile
    capture.apply_on_packets(decode_romulus_frame)
    capture.sniff()

    if(len(capture) == 0):
        raise ValueError('No packets were captured!')

    return capture

def printByte(string):
     byte = string.replace('bytearray(b\'', "").replace('\')','')
     return '0-'+byte+'\n'


def decode_tcp(packet):
    if(packet.transport_layer == 'TCP'):
        tcp = packet.tcp
        ip = packet.ip
        eth = packet.eth

        try:
            src = '%s:%s\t%s' % (ip.src_host, tcp.srcport, ip.geoasnum)
            dst = '%s:%s\t%s' % (ip.dst_host, tcp.dstport, ip.geodst_asnum)

            route = 'Packet %d: %s\nfrom %s\nto %s' % (cnt, packet.layers, src, dst)
            geo   = '%s %s -> %s %s' % (ip.geocountry, ip.geocountry, ip.geodst_city, ip.geodst_country)
            info  = 'IP version = %s, ttl = %s,' % (ip.version, ip.ttl)

            print('%s\n%s\n%s\n' % (route, info, geo))
            
        
        except:
            print('Packet %d is missing some attributes\n' % (cnt))
    else:
        print('Packet %d is no tcp packet\n' % (cnt))
    

class romulus_frame:
    def __init__(self):
        self.length = 0
        self.raw = 0

    def __len__(self):
        return self.length

    def __str__(self):
        # frame_tuple = (self.rstart, self.rdlc, self.rid_source, self.rseq_id, self.rcmd_code, self.rnp, self.rdata, self.rchksum, self.rend)
        frame_tuple = (self.dlc, self.rid_source, self.seq_id, self.cmd_code, self.np, self.data, self.chksum)

        string = "{}-{}-{}-{}-{}-{}-{}".format(*frame_tuple)
        string.replace("bytearray(b","")
        return string

    def parse_hexdata(self, data):
        payload = bytearray.fromhex(data.replace(':', ''))
        
        self.length = len(payload)
        self.raw = payload

        self.dlc       = ba.hexlify(payload[0:2])
        self.id_source = ba.hexlify(payload[2:18])
        self.seq_id    = ba.hexlify(payload[18:20])
        self.cmd_code  = ba.hexlify(payload[20:22])
        self.np        = ba.hexlify(payload[22:23])
        self.data      = ba.hexlify(payload[23:-2])
        self.chksum    = ba.hexlify(payload[-2:])

        self.rdlc       = payload[0:2]
        self.rid_source = payload[2:18]
        self.rseq_id    = payload[18:20]
        self.rcmd_code  = payload[20:22]
        self.rnp        = payload[22:23]
        self.rdata      = payload[23:-2]
        self.rchksum    = payload[-2:]

    def raw(self):
        return self.raw

    def decode(self):
        decoded_frame = romulus_frame()
        


def decode_romulus_frame(packet):
    if(packet.transport_layer == 'TCP' and packet.highest_layer == 'DATA'):
        payload = packet.data.data
        frame = romulus_frame();
        frame.parse_hexdata(payload)

        new_packet = str(frame)
        sys.stdout.write(printByte(new_packet))
        sys.stdout.flush()

        sys.stderr.write("")
        sys.stderr.flush()

class N:
    pass

def parse_arguments():
  
    parser = argparse.ArgumentParser(description='Packet sniffer for CERN\'s CROME device.')
    arguments = ['-i', '-bpf', '-dump']

    for arg in arguments:
        parser.add_argument(arg)

    n = N()
    parser.parse_args(namespace=n)

    return n


def main():

    args = parse_arguments()
    (interface, bpf, dumpfile) = (args.i, args.bpf, args.dump)
    arguments = (interface, bpf, dumpfile)

    start_sniffing(interface, bpf, dumpfile)
     

if __name__ == "__main__":
    main()