#ifndef SNIFFER_H
#define SNIFFER_H
#include <wrapper.h>

#include <QMap>
#include <QObject>
#include <QPair>
#include <QDebug>

class Sniffer : public Wrapper
{
    Q_OBJECT
public:
    Sniffer(QString prog);

private:
    int counter;
    QMap<int, QStringList> packets;
    QStringList parseFrame(QString);

signals:
    void sendDataframe(QStringList);
    void packetArrived(int);

public slots:
    void getDataframe(int);
    void addPacket(QString);
    void clearContainer();

};

#endif // SNIFFER_H
